package com.pizzagenerator;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class App extends Application {
    public static void main(String[] args) {
        launch(args);
    }


    @Override
    public void start(Stage primaryStage) throws IOException {
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/fxml/App.fxml"));
        Parent root = loader.load();
        Main controller = loader.getController();
        primaryStage.setTitle("Pizza Application");
        primaryStage.setResizable(false);
        primaryStage.setScene(new Scene(root, 676, 645));
        primaryStage.show();
        controller.getCSVFileContent();
    }
}
