package com.pizzagenerator;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pizzagenerator.model.FileMetaData;
import com.pizzagenerator.model.Items;
import com.pizzagenerator.model.PizzaData;
import com.pizzagenerator.model.ServerData;
import javafx.animation.PauseTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Duration;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Main {

    @FXML
    public Button btnAdd;
    @FXML
    private TabPane pizzaPain;
    @FXML
    private Tab deliverTab;
    @FXML
    private ComboBox<String> cbItems;
    @FXML
    private TextField txtQuantity;
    @FXML
    private Button btnOrder;
    @FXML
    private TableColumn<PizzaData, String> amountColumn;
    @FXML
    private TableColumn<PizzaData, String> itemColumn;
    @FXML
    private TableColumn<PizzaData, Double> priceColumn;
    @FXML
    private TableView<PizzaData> orderTable;
    @FXML
    private Label total;
    @FXML
    private Button btnEndOrder;
    @FXML
    private TextField adressTextField;
    @FXML
    private TextField cityTextField;
    @FXML
    private TextField nameTextField;
    @FXML
    private TextField postalTextField;
    @FXML
    private Label errorLabel;


    private void postReceipt() {

        final CloseableHttpClient client = HttpClients.createDefault();
        final HttpPost httpPost = new HttpPost("http://127.0.0.1:8080/api/files/create-file");

        Gson gson = new Gson();

        // Tworzymy obiekt uzytkownika
        final ServerData orderData = new ServerData(nameTextField.getText(), adressTextField.getText(),
                cityTextField.getText(), postalTextField.getText(),orderList,discount,totalPrice);

        // Serializacja obiektu do JSONa
        final String json = gson.toJson(orderData);

        try {

            final StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setHeader("accept-charset", "UTF-8");

            final CloseableHttpResponse response = client.execute(httpPost);

            System.out.println("Kod odpowiedzi serwera: " + response.getStatusLine().getStatusCode());

            if(response.getStatusLine().getStatusCode() == 404) {

                System.out.println("Prosze poprawic w kontrolerze sciezke do pliku - sciezka jest nieprawidlowa!");
            } else if(response.getStatusLine().getStatusCode() == 201) {

                final HttpEntity httpEntity = response.getEntity();

                // Na tym etapie odczytujemy JSON'a, ale jako String.
                final String jsonAsFileMetaData = EntityUtils.toString(httpEntity);

                // Wyswietlamy zawartosc JSON'a na standardowe wyjscie.
                System.out.println("Odczytujemy JSON'a z serwera:");
                System.out.println(jsonAsFileMetaData);

                final FileMetaData fileMetaData = gson.fromJson(jsonAsFileMetaData, FileMetaData.class);

                // Dzialamy na obiekcie - mamy dostep do danych, ktore zostaly odczytane z JSON'a
                System.out.println("Dane zapisanego pliku:");
                System.out.printf("Data utworzenia pliku: %s \n", fileMetaData.getCreationDate());
                System.out.printf("Nazwa pliku: %s \n", fileMetaData.getFileName());
                System.out.printf("Rozmiar pliku: %d B \n", fileMetaData.getSize());
            }

            client.close();
        } catch (UnsupportedEncodingException e) {

            System.out.println("Houston, we have a problem with POST unsupported encoding");
            e.printStackTrace();
        } catch (ClientProtocolException e) {

            System.out.println("Houston, we have a problem with POST client protocol");
            e.printStackTrace();
        } catch (IOException e) {

            System.out.println("Houston, we have a problem with POST input output");
            e.printStackTrace();
        }
    }

    public void getCSVFileContent() {
        final HttpClient client = HttpClientBuilder.create().build();

        /*
            Do konstruktora klasy HttpGet podajemy url z nasza usluga ktora zwaraca JSON'a.
            W tym miejscu tworzymy request serwera.
        */
        final HttpGet request = new HttpGet("http://127.0.0.1:8080/api/files/find-all");

        /* Przy pomocy tej biblioteki zmienimy JSON'a na obiekt typu 'FileMetaData'. */
        final Gson gson = new Gson();

        try {

            final HttpResponse response = client.execute(request);
            final HttpEntity entity = response.getEntity();

            final String json = EntityUtils.toString(entity);

            // Wyswietlamy zawartosc JSON'a na standardowe wyjscie.
            System.out.println("Odczytujemy JSON'a z serwera:");
            System.out.println(json);

            /*
                Tutaj odbywa sie przetworzenie (serializacja) JSON'a (String) na List<FileMetaData>
                Prosze przeanalizowac jak wyglada struktura klasy FileMetaData. Struktura klasy dokladnie
                odwozorowywuje strukture JSON'a zwroconego przez serwer dlatego biblioteka jest w stanie
                poradzic sobie z taka konstrukcja
             */

            final Type type = new TypeToken<ArrayList<PizzaData>>() {
            }.getType();
            final List<PizzaData> files = gson.fromJson(json, type);

            /*
                Jestesmy w stanie odczytac kod odpowiedzi serwera.
                Kod odpowiedzi zostanie wyswietlony na standardowym wyjsciu.
            */
            System.out.println("Kod odpowiedzi serwera: " + response.getStatusLine().getStatusCode());

            if (response.getStatusLine().getStatusCode() == 404) {

                System.out.println("Brak danych do wyswietlenia!");
            } else if (response.getStatusLine().getStatusCode() == 200) {

                // Dzialamy na obiekcie - mamy dostep do danych, ktore zostaly odczytane z JSON'a
                System.out.println("Lista plikow:");
                for (final PizzaData file : files) {
                    System.out.printf("Plik: %s, Price %s zł\n", file.getName(), file.getPrice());
                    Items.addItem(file.getName(), file.getPrice(), txtQuantity.getText());
                    dropdownList.add(file.getName());
                }
                initialize();
            }

        } catch (IOException e) {

            System.out.println("Houston, we have a problem with GET");
            e.printStackTrace();
        }

    }

    public HashMap<String, Integer> orderList = new HashMap<>();
    private double subTotal = 0;
    private double discount = 0;
    private int pizzaCounter = 0;
    private double totalPrice = 0;


    // Lista wszystkich produktów w ofercie pizzeri
    public ArrayList<PizzaData> exprimentOrderList = new ArrayList<>();

    // Lista wszystkich produktów w ofercie pizzeri które może wybrać użytkownik
    ObservableList<String> dropdownList = FXCollections.observableArrayList(Items.items.keySet());


    public void initialize() {
        pizzaPain.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        txtQuantity.setText("1");

        // przypisz zmienne do kolumn w TableView
        itemColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        amountColumn.setCellValueFactory(new PropertyValueFactory<>("amount"));
        priceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));


        // Tworzy comboList elementów, które są obecnie w ofercie pizzeri
        cbItems.setItems(dropdownList);

        // Zaznacza domyślnie pierwszy produkt z listy
        cbItems.getSelectionModel().selectFirst();

    }

    @FXML
    public void addItem() {

        // Pobiera wybrany przedmiot z listy
        String text = cbItems.getSelectionModel().getSelectedItem();
        int amount = Integer.parseInt(txtQuantity.getText()); //hashmap version


        if (orderList.containsKey(text)) {
            orderList.put(text, orderList.get(text) + amount);
        } else {
            orderList.put(text, amount);
        }

        // Tworzy nowy typ dla posiłków i napojów gratisowych
        PizzaData item = new PizzaData(text, Items.getItemPrice(text), amount + "");
        PizzaData freeDrink = new PizzaData("Napoj", 0, "1");
        // dodaje produkt do zamówienia
        exprimentOrderList.add(item);
        subTotal += Items.getItemPrice(text) * (amount);
        if (subTotal > 100) {
            discount = (subTotal * 0.2);
        }
        // dodaje wybrany produkt do tablicy
        orderTable.getItems().add(item);

        totalPrice = subTotal - Math.round(discount);

        // pomocnicza zmienna zliczająca ilość pizzy
        if (item.getName().contains("Pizza")) {
            pizzaCounter += amount;
        }
        if (pizzaCounter > 0 && pizzaCounter % 2 == 0) {
            exprimentOrderList.add(freeDrink);
            if (orderList.containsKey("Napoj")) {
                orderList.put("Napoj", orderList.get("Napoj") + 1);
            } else {
                orderList.put("Napoj", 1);
            }
            orderTable.getItems().add(freeDrink);
            System.out.printf("Pizzacounter: %d - Free drink added\n", pizzaCounter);
        }
        if (pizzaCounter > 0 && amount > 1) {
            int drinkCounter = (int) (Math.floor((pizzaCounter / 2)));
            for (int i = 1; i <= drinkCounter; i++) {
                exprimentOrderList.add(freeDrink);
                orderTable.getItems().add(freeDrink);
                if (orderList.containsKey("Napoj")) {
                    orderList.put("Napoj", orderList.get("Napoj") + 1);
                } else {
                    orderList.put("Napoj", 1);
                }
            }
            System.out.printf("Pizzacounter: %d - Free drinks added\n", drinkCounter);
        }
        // Aktualizuje cenę
        total.setText("" + totalPrice);
        System.out.println(orderList);


        // zabezpieczenie przed pustym zamówieniem
        if (!orderTable.getItems().isEmpty()) {
            deliverTab.setDisable(false);
            btnOrder.setDisable(false);
            btnEndOrder.setDisable(false);
        }
    }

    @FXML
    private void changeTab() {
        pizzaPain.getSelectionModel().select(deliverTab);
    }

    @FXML
    void endOrder() {
        if (!adressTextField.getText().isEmpty() || !cityTextField.getText().isEmpty() ||
                !postalTextField.getText().isEmpty() || !nameTextField.getText().isEmpty()) {
            if (!postalTextField.getText().matches(".*\\d.*") || !nameTextField.getText().contains(" ")
                    || cityTextField.getText().matches(".*\\d.*") || nameTextField.getText().matches(".*\\d.*")) {
                showError();
            } else {
                System.out.printf("Zamówienie: : %s, %s, %s, %s\n",
                        adressTextField.getText(), cityTextField.getText(), postalTextField.getText(), nameTextField.getText());
                postReceipt();
            }
        } else {
            showError();
        }
    }

    private void showError() {
        errorLabel.setText("Sprawdź poprawność danych");
        errorLabel.setVisible(true);
        PauseTransition pause = new PauseTransition(Duration.seconds(2));
        pause.setOnFinished(e -> errorLabel.setVisible(false));
        pause.play();
    }
}


