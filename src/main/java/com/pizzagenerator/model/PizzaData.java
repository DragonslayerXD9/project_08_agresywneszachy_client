package com.pizzagenerator.model;

public class PizzaData {

    private String amount;
    private double price;
    private String name;


    public PizzaData(String name, double price, String amount) {
        this.amount = amount;
        this.price = price;
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getAmount() {
        return amount;
    }

    public void setAmount(String size) {
        this.amount = amount;
    }

}
