package com.pizzagenerator.model;

import java.util.ArrayList;
import java.util.HashMap;

public class ServerData {

    private String name;
    private String street;
    private String city;
    private String postcode;
    private HashMap<String, Integer> orderList;
    double discount;
    double totalPrice;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public ServerData(String name, String street, String city, String postcode,
                      HashMap<String,Integer> orderList, double discount, double totalPrice) {
        this.name = name;
        this.street = street;
        this.city = city;
        this.postcode = postcode;
        this.orderList = orderList;
        this.discount = discount;
        this.totalPrice = totalPrice;
    }
}
