package com.pizzagenerator.model;

import java.util.HashMap;

public class Items {

    public static HashMap<String, Double> items = new HashMap<String, Double>();
    public static HashMap<String, PizzaData> itemObjects = new HashMap<String, PizzaData>();

    public static void addItem(String name, double price, String txtQuantity) {
        items.put(name, price);
        System.out.printf("addItem: %s, Price %f zł\n", name, price);

        // dodaje obiekt
        itemObjects.put(name, new PizzaData(name, price, txtQuantity));
    }

    public static HashMap<String, Double> getItems() {
        return items;
    }

    public static void removeItem(String name) {
        items.remove(name);

        // usuwa obiekt
        itemObjects.remove(name);

        System.out.printf("ITEM REMOVED: %s", name);
    }

    static HashMap<String, PizzaData> getItemObjects() {
        return itemObjects;
    }

    public static double getItemPrice(String price) {
        return items.get(price);
    }
}
